from models.grouped_tiles_net import build_model
from keras.optimizers import Adam
from data_loaders import _list_all_training_fnames, PATH_DATA
from sklearn.model_selection import train_test_split
from data_generators import DataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint
import numpy as np


# Split the available filenames into train/validation
all_fnames = _list_all_training_fnames()
indices_train, indices_val = train_test_split(np.arange(len(all_fnames)),
                                              test_size=0.3,
                                              random_state=42)

training_fnames = [all_fnames[j] for j in indices_train]
validation_fnames = [all_fnames[j] for j in indices_val]

# Create generators
batch_size = 4
train_gen = DataGenerator(batch_size=batch_size,
                          fnames=training_fnames,
                          shuffle=True)
val_gen = DataGenerator(batch_size=batch_size,
                        fnames=validation_fnames,
                        shuffle=True)

# Instantiate and fit the model
model = build_model(n_features=2048)
print(model.summary())

optimizer = Adam(lr=0.00005)
model.compile(optimizer=optimizer,
              loss='binary_crossentropy',
              metrics=['accuracy'])

earlystop = EarlyStopping(monitor='val_loss',
                          patience=5,
                          verbose=1,
                          mode='auto')

modelcheckpoint = ModelCheckpoint(
    filepath=str(PATH_DATA.joinpath('saved_models', 'model.{epoch:02d}.hd5')),
    monitor='val_loss',
    save_best_only=True,
    mode='auto',
    period=1,
    verbose=0)

history = model.fit_generator(train_gen,
                              epochs=50,
                              validation_data=val_gen,
                              callbacks=[modelcheckpoint, earlystop])

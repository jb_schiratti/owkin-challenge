"""Utility function for results submission."""

import pandas as pd


def create_submission(df, fname):
    """Creates a CSV file with test results.

    Parameters
    ----------
    df : pd.DataFrame
        DataFrame with predicted probabilities. The DataFrame should have,
        as expected, two columns: `ID` and `Target`.

    fname : str or instance of pathlib.Path
        Filename of the CSV file.
    """
    df = df.sort_values(by='ID')
    df.to_csv(path_or_buf=fname,
              sep=",",
              index=False)

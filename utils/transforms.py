"""Utility functions to transform predicted class probabilities."""

from sklearn.preprocessing import FunctionTransformer
from sklearn.pipeline import FeatureUnion
from scipy.stats import hmean, gmean
from scipy.special import logit
import numpy as np


def _clip(x):
    """Clip values to (0,1)."""
    return np.clip(x, a_min=1e-12, a_max=1 - 1e-12)


def probabilities_transformer():
    """Creates a transformer for tile probabilities.

    Returns
    -------
    transformer : instance of `sklearn.pipeline.FeatureUnion`
    """
    tr1 = FunctionTransformer(func=lambda x: hmean(_clip(1 - x)),
                              validate=False)
    tr2 = FunctionTransformer(func=lambda x: 1 - gmean(_clip(1 - x)),
                              validate=False)
    tr3 = FunctionTransformer(func=lambda x: np.mean(x),
                              validate=False)
    tr4 = FunctionTransformer(func=lambda x: np.median(logit(_clip(x))),
                              validate=False)
    transformer = FeatureUnion(transformer_list=[('hmean', tr1),
                                                 ('gmean', tr2),
                                                 ('mean', tr3),
                                                 ('max_logit', tr4)])
    return transformer

"""Utility functions to load [training or test] data as Numpy arrays."""

from pathlib import Path

import numpy as np
import pandas as pd

PATH_DATA = Path('C:\data\ChallengeOwkin\ResNetFeatures')
PATH_WSI_ANNOTATIONS = PATH_DATA.joinpath('train_output.csv')
PATH_TILE_ANNOTATIONS = PATH_DATA.joinpath('train_input',
                                           'train_tile_annotations.csv')


def _load_wsi_labels():
    wsi_labels = pd.read_csv(filepath_or_buffer=PATH_WSI_ANNOTATIONS)
    wsi_labels['ID'] = wsi_labels['ID'].apply(lambda s: s.split('_')[1])
    wsi_labels = wsi_labels.set_index('ID')
    return wsi_labels


def _load_tiles_labels():
    tiles_labels = pd.read_csv(filepath_or_buffer=PATH_TILE_ANNOTATIONS,
                               names=['Filename', 'Target'],
                               header=0)
    tiles_labels['Tile_ID'] = tiles_labels['Filename'].apply(
        lambda s: int(s.split('_')[4]))
    tiles_labels['ID'] = tiles_labels['Filename'].apply(
        lambda s: s.rsplit('_', 6)[0])
    tiles_labels = tiles_labels.set_index('ID')
    return tiles_labels


def _list_all_training_fnames():
    return [f for f in PATH_DATA.joinpath('train_input',
                                          'resnet_features').iterdir()
            if f.is_file()]


def get_training_resnet_features_fnames():
    """Lists all the ResNet features files available for training.

    Returns
    -------
    fnames_strong_annot : list of str or instances of pathlib.Path
        List of filenames with string (tile-level) annotations.

    fnames_weak_annot : list of str or instances of pathlib.Path
        List of filenames with weak (only slide-level) annotations.
    """
    all_fnames = _list_all_training_fnames()
    fnames_strong_annot = [f for f in all_fnames if 'annot' in f.name]
    fnames_weak_annot = [f for f in all_fnames if 'annot' not in f.name]
    return fnames_strong_annot, fnames_weak_annot


def get_test_resnet_features_fnames():
    """Lists all the ResNet features files available for testing.

    Returns
    -------
    fnames : list of str or instances of pathlib.Path
    """
    fnames = [f for f in PATH_DATA.joinpath('test_input',
                                            'resnet_features').iterdir()
              if f.is_file()]
    return fnames


def get_strongly_annotated_training_data():
    """Extract features and labels from strongly annotated files.

    Returns
    -------
    X : ndarray, shape (n_annotated_samples, n_features)
        ResNet features for strongly annotated tiles.

    y : ndarray, shape (n_annotated_samples,)
        Corresponding labels.

    group : ndarray, shape (n_annotated_samples,)
        Array containing the ID of each sample.
    """
    fnames, _ = get_training_resnet_features_fnames()

    # Load annotations from CSV file
    tiles_labels = _load_tiles_labels()

    # Get features and labels
    X, y, group = [], [], []
    for fname in fnames:
        features = np.load(fname)
        current_id = fname.stem.split('_')[1]
        aux_df = tiles_labels.loc['ID_' + current_id, :]
        # Ensure that all the tiles are sorted by 'Tile_ID'
        aux_df = aux_df.sort_values(by='Tile_ID')
        labels = aux_df['Target'].values

        X.append(features[:, 3:])
        y.append(labels)
        group.append(int(current_id) * np.ones((features.shape[0],)))

    X = np.concatenate(X, axis=0)
    y = np.concatenate(y, axis=0)
    group = np.concatenate(group, axis=0)
    return X, y, group


def get_weakly_annotated_training_data(fname):
    """Extracts ResNet features and labels from a single weakly annotated file.

    Parameters
    ----------
    fname : str or instance of pathlib.Path
        Filename.

    Returns
    -------
    X : ndarray, shape (n_annotated_samples, n_features)
        ResNet features for weakly annotated tiles.

    y : int
        Corresponding slide-level label.
    """
    # Get annotations from CSV file
    wsi_labels = _load_wsi_labels()

    # Get features and labels
    features = np.load(fname)
    current_id = fname.stem.split('_')[1]
    slide_label = wsi_labels.loc[current_id, 'Target']

    return features[:, 3:], slide_label

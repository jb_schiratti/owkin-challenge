# Owkin breast cancer challenge

This document describes the different ideas I had regarding this challenge and discusses the ones I have implemented.
 
## Idea \#1:

We are given weak labels (slide-level labels ; one label for ~1000 tiles) and strong labels (tile-level labels ; one label per tile).
If we want to classify tiles individually, the strong labels are likely to be more reliable than the weak ones.
Therefore, the first idea consists in improving the baseline along these lines:

1. Fit a XGBoost classifier to the ResNet features of patient IDs who we have strong labels,
2. Use the fitted XGBoost classifier to predict the class probabilities of each tile using the ResNet features of other patient IDs.

    As a result of steps 1. and 2., if a patient ID (with weak labels) has ResNet features with shape `(ni_tiles, n_features)`, we shall obtain an array with shape `(ni_tiles,)`, each value corresponding to the probability that a tile belongs to the positive class.
3. For each patient ID (with weak labels), the `(ni_tiles,)` array of predicted probabilities is transformed into an array with shape `(n_handcrafted_features,)`. This step basically transforms an array of probabilities (whose shape depends on the ID) into a new array of handcrafted features with constant size.
4. The handcrafted features along with the weak labels are used to train a final classifier (called `final_clf`) which pools together the handcrafted features and predicts the probability that a set of tiles contains a metastase.

These steps are implemented in `train_xgb_classifier.py`. The XGB classifier is defined in `models/optimized_xgb.py`, where the `hyperopt` Python library is used to fine-tune some of the hyperparameters of the XGBoost classifier. Once the classifiers trained, they are used to make predictions on test data in `predict_xgb_classifiers.py`.

#### Comments:

Although this approach may seem quite naive, it yields a ROC AUC score of **0.8264** on the public leaderboard of the competition. Perspectives of improvement may include:

* Using a smarter method of pooling/combining the tile probabilities together instead of using handcrafted features,
* consider using a different feature extractor (other than ResNet50 although it yields a good compromise between complexity, computational cost and performance),
* ...

## Idea \#2:

It consists in implementing the method proposed in [1]. In this CVPR2019 paper, the authors propose a neural network to perform image classification in a weakly supervised setting. Usually, those methods only assume that a fraction of the available samples have noisy labels and try to address this problem by introducing new loss functions or estimate label-fliping probabilities (see [2], for example). Here, this paper assumes that one can differentiate between samples with "reliable" labels (clean data) and those with noisy/unreliable labels (noisy data). The neural network proposed in [1] consists of a shared feature extractor (they used ResNet50), a "clean net" (trained using clean data) and a "residual net" (trained using the noisy data and the output of the \[last layer of\] clean net). This "residual net" acts as a "noise regularization" for the "clean net" and improves the training of the "clean net". With this approach, the authors report state-of-the-art performances on a large (Clothing1M) single-label image dataset.

The method (and neural network) proposed in this paper seemed interesting since they allow to take advantage of the strong labels (tile-level labels) available for 11 of the patient IDs (in the training set).Two implementations of this neural network are proposed in `models/clean_residual_net.py`.

#### Comments:

The Keras implementation of this neural network requires that the "clean" data and the "noisy" data have the same number of samples. However, in the training set, we have 10124 images with "clean" labels and 240641 images with "noisy" labels. Therefore, one could perform _data augmentation_ on the "clean" images in order to increase the size of the dataset before training this network. 

Before training this network, the remaining steps would be:

1. Perform data augmentation on the images with "clean" labels in order to have the same number of images with "clean" labels than images with "noisy" labels. For histopathology images, data augmentation may consists in: flipping the image vertically (makes sense from the point of view of the clinician), random fixed-size crop, changing the brightness, histogram equalization... These steps can easily be implemented using, for example, the `skimage` library. 

2. Write the data generators to train the model. This can be done using the `ImageDataGenerator` class provided in `keras.preprocessing.image`.

## Other ideas I would like to explore:

* The method proposed in [5] is also quite interesting. I would like to give it a try.
## References:

    [1] Hu, M., Han, H., Shan, S., & Chen, X. (2019). Weakly Supervised Image Classification Through Noise Regularization. In Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition (pp. 11517-11525).

    [2] https://github.com/BMIRDS/deepslide
    
    [3] Wei, J. W., Tafe, L. J., Linnik, Y. A., Vaickus, L. J., Tomita, N., & Hassanpour, S. (2019). Pathologist-level classification of histologic patterns on resected lung adenocarcinoma slides with deep neural networks. Scientific reports, 9(1), 3358

    [4] Liu, Y., Kohlberger, T., Norouzi, M., Dahl, G. E., Smith, J. L., Mohtashamian, A., ... & Stumpe, M. C. (2018). Artificial Intelligence–Based Breast Cancer Nodal Metastasis Detection: Insights Into the Black Box for Pathologists. Archives of pathology & laboratory medicine
    
    [5] Durand, T., Thome, N., & Cord, M. (2016). Weldon: Weakly supervised learning of deep convolutional neural networks. In Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition (pp. 4743-4752).

Note that - unless I am mistaken - [2], [3], [4] are designed to work in a strongly supervised setting.
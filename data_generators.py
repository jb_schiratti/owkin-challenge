"""Utility functions to create generators for Keras models."""

import numpy as np
from keras.utils import Sequence
from data_loaders import _load_wsi_labels
from math import ceil
from keras.utils import to_categorical


class DataGenerator(Sequence):

    def __init__(self, batch_size, fnames, shuffle=True):
        """Initialize the data generator"""
        self.batch_size = batch_size
        self.fnames = fnames
        self.shuffle = shuffle
        self._train_info = _load_wsi_labels()
        self.on_epoch_end()

    def __len__(self):
        """Number of batches in each epoch."""
        return int(ceil(len(self.fnames) / self.batch_size))

    def __getitem__(self, index):
        """Generates a batch"""
        batch_indexes = self.indexes[
                        index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        selected_fnames = [self.fnames[k] for k in batch_indexes]

        # Generate data
        X, y = self.__data_generation(selected_fnames)

        return X, y

    def on_epoch_end(self):
        """Update indexes at the end of each epoch."""
        self.indexes = np.arange(len(self.fnames))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, selected_fnames):
        """Generate data for a given batch"""
        X, y = [], []
        for fname in selected_fnames:
            features = np.load(fname)
            selected_id = fname.stem.split('_')[1]
            label = self._train_info.loc[selected_id, 'Target']

            # pad if there are not 1000 tiles
            if features.shape[0] < 1000:
                _padded = np.pad(features,
                                 [(0, 1000 - features.shape[0]), (0, 0)],
                                 mode='constant')
            else:
                _padded = features

            X.append(_padded[:, 3:])
            y.append(label)

        # Return X, y
        X, y = np.asarray(X), np.asarray(y)
        return X[..., None], to_categorical(y, num_classes=2)

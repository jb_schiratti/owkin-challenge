"""Keras implementation of deep learning model proposed in [1].

References
----------
.. [1] Hu, M., Han, H., Shan, S., & Chen, X. (2019). Weakly Supervised Image
       Classification Through Noise Regularization. In Proceedings of the
       IEEE Conference on Computer Vision and Pattern Recognition
       (pp. 11517-11525).
"""

from keras.applications import ResNet50
from keras.layers import (Input, Dense, Dropout, ReLU, Add, Reshape,
                          Conv1D, BatchNormalization, Flatten)
from keras.models import Model


def build_model(img_height, img_width, img_channels):
    """Naive implementation of the model proposed in [1].

    Parameters
    ----------
    img_height : int
        Height of the images fed to the model.

    img_width : int
        Width of the images fed to the model.

    img_channels : int
        Number of channels per image. The parameter `img_channels` is expected
        to be equal to 3 for color images (e.g. RGB channels).

    Returns
    -------
    model : instance of `keras.engine.training.Model`.
    """
    input_shape = (img_height, img_width, img_channels)

    input_clean = Input(shape=input_shape, name='input_clean')
    input_noisy = Input(shape=input_shape, name='input_noisy')

    resnet_layer = ResNet50(weights='imagenet',
                            include_top=False,
                            pooling='avg')
    out_resnet_clean = resnet_layer(input_clean)
    out_resnet_noisy = resnet_layer(input_noisy)

    out1_clean = Dense(units=resnet_layer.output_shape[-1] // 8)(
        out_resnet_clean)
    out2_clean = ReLU()(out1_clean)
    out3_clean = Dense(units=resnet_layer.output_shape[-1] // 64)(out2_clean)
    output_clean = Dense(units=2,
                         activation='softmax',
                         name='output_clean')(out3_clean)

    out1_noisy = Dense(units=resnet_layer.output_shape[-1] // 8)(
        out_resnet_noisy)
    out2_noisy = ReLU()(out1_noisy)
    out3_noisy = Dense(units=resnet_layer.output_shape[-1] // 64)(out2_noisy)
    out4_noisy = Add()([out3_noisy, out3_clean])
    output_noisy = Dense(units=2,
                         activation='softmax',
                         name='output_noisy')(out4_noisy)

    # Return model
    model = Model(inputs=[input_noisy, input_clean],
                  outputs=[output_noisy, output_clean])
    return model


def build_model2(img_height, img_width, img_channels):
    """(Less naive) Implementation of the model proposed in [1].

    Parameters
    ----------
    img_height : int
        Height of the images fed to the model.

    img_width : int
        Width of the images fed to the model.

    img_channels : int
        Number of channels per image. The parameter `img_channels` is expected
        to be equal to 3 for color images (e.g. RGB channels).

    Returns
    -------
    model : instance of `keras.engine.training.Model`.
    """
    input_shape = (img_height, img_width, img_channels)

    input_clean = Input(shape=input_shape, name='input_clean')
    input_noisy = Input(shape=input_shape, name='input_noisy')

    resnet_layer = ResNet50(weights='imagenet',
                            include_top=False,
                            pooling='avg')
    out_resnet_clean = resnet_layer(input_clean)
    out_resnet_noisy = resnet_layer(input_noisy)

    reshaped_clean_input = Reshape(
        target_shape=(resnet_layer.output_shape[-1], 1))(out_resnet_clean)
    out1_clean = Conv1D(128,
                        kernel_size=resnet_layer.output_shape[-1],
                        strides=1,
                        padding='valid',
                        name='embed_1d_clean')(reshaped_clean_input)
    out2_clean = BatchNormalization()(out1_clean)
    out3_clean = ReLU()(out2_clean)
    out4_clean = Flatten()(out3_clean)
    out5_clean = Dense(units=32)(out4_clean)
    out6_clean = Dropout(0.5)(out5_clean)
    output_clean = Dense(units=2,
                         activation='softmax',
                         name='output_clean')(out6_clean)

    reshaped_noisy_input = Reshape(
        target_shape=(resnet_layer.output_shape[-1], 1))(out_resnet_noisy)
    out1_noisy = Conv1D(128,
                        kernel_size=resnet_layer.output_shape[-1],
                        strides=1,
                        padding='valid',
                        name='embed_1d_noisy')(reshaped_noisy_input)
    out2_noisy = BatchNormalization()(out1_noisy)
    out3_noisy = ReLU()(out2_noisy)
    out4_noisy = Flatten()(out3_noisy)
    out5_noisy = Dense(units=32)(out4_noisy)
    out6_noisy = Dropout(0.5)(out5_noisy)
    out7_noisy = Add()([out6_noisy, out6_clean])
    output_noisy = Dense(units=2,
                         activation='softmax',
                         name='output_noisy')(out7_noisy)

    # Return model
    model = Model(inputs=[input_noisy, input_clean],
                  outputs=[output_noisy, output_clean])
    return model

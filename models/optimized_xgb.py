"""XGBClassifier with parameters optimized using hyperopt."""

import numpy as np
from hyperopt import fmin, tpe, hp, Trials, STATUS_OK
from sklearn.base import BaseEstimator, ClassifierMixin, clone
from sklearn.exceptions import NotFittedError
from sklearn.metrics import f1_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.utils.class_weight import compute_sample_weight
from xgboost import XGBClassifier


def optimize_params(X, y, params_space, validation_split, n_jobs,
                    class_weight, verbose):
    """Utility function to estimate a set of 'best' model parameters."""

    # Estimate XGB params
    def _objective(_params):
        scores = list()
        base_clf = XGBClassifier(n_estimators=5000,
                                 max_depth=int(_params['max_depth']),
                                 learning_rate=_params['learning_rate'],
                                 min_child_weight=_params['min_child_weight'],
                                 subsample=_params['subsample'],
                                 colsample_bytree=_params['colsample_bytree'],
                                 gamma=_params['gamma'],
                                 n_jobs=n_jobs)
        _cv = StratifiedShuffleSplit(n_splits=5, test_size=validation_split)
        for idx_train, idx_val in _cv.split(
                X=np.arange(y.shape[0], dtype=np.int32), y=y):
            X_train, y_train = X[idx_train, ...], y[idx_train]
            X_val, y_val = X[idx_val, ...], y[idx_val]
            _clf = clone(base_clf)

            # sample_weights
            if class_weight is not None:
                _weights_train = compute_sample_weight(class_weight, y_train)
                _weights_eval = compute_sample_weight(class_weight, y_val)
            else:
                _weights_train = None
                _weights_eval = None

            _clf.fit(X_train, y_train,
                     sample_weight=_weights_train,
                     eval_set=[(X_train, y_train), (X_val, y_val)],
                     sample_weight_eval_set=[_weights_train, _weights_eval],
                     eval_metric='auc',
                     early_stopping_rounds=10,
                     verbose=verbose)
            y_pred = _clf.predict(X_val)
            score = f1_score(y_val, y_pred)
            scores.append(score)
        return {'loss': 1. - np.mean(scores), 'status': STATUS_OK}

    trials = Trials()
    return fmin(fn=_objective,
                space=params_space,
                algo=tpe.suggest,
                max_evals=100,
                trials=trials,
                verbose=0)


class OptimizedXGB(BaseEstimator, ClassifierMixin):
    """XGBoost model with Scikit-Learn API and {'learning_rate', 'max_depth',
    'min_child_weight', 'subsample', 'gamma', 'colsample_bytree'} parameters
    optimized using the hyperopt library.

    Parameters
    ----------
    custom_params_space : dict or None
        If not None, dictionary whose keys are the XGB parameters to be
        optimized and corresponding values are 'a priori' probability
        distributions for the given parameter value. If None, a default
        parameters space is used.

    n_jobs : int (default: 1)
        Number of CPUs used to train the XGB models. If given a value of
        -1, all CPUs are used.

    class_weight : dict or str or None (default: None)
        Weights associated with the classes. If dict, `class_weight` should be
        of the form `{class_label: weight}`. If str, only `'balanced'` is
        accepted. If None, no sample weight is used.

    verbose : bool (default: False)
        If True, the XGB models will print progress information during
        training. Note that this might result in a lot of information printed
        during training.
    """
    def __init__(self, custom_params_space=None, n_jobs=1,
                 class_weight=None, verbose=False):
        self.custom_params_space = custom_params_space
        self.n_jobs = n_jobs
        self.class_weight = class_weight
        self.verbose = verbose

    def fit(self, X, y, validation_split=0.3):
        """Train a XGB model.

        Parameters
        ----------
        X : ndarray, shape (n_samples, n_features)
            Data.

        y : ndarray, shape (n_samples,) or (n_samples, n_labels)
            Labels.

        validation_split : float (default: 0.3)
            Float between 0 and 1. Corresponds to the percentage of samples in
            X which will be used as validation data to estimate the 'best'
            model parameters.

        Returns
        -------
        self
        """
        # If no custom parameters space is given, use a default one.
        _space = {
            'colsample_bytree': hp.quniform('colsample_bytree', 0.4, 1,
                                            0.05),
            'min_child_weight': hp.quniform('min_child_weight', 0.01, 10,
                                            0.01),
            'subsample': hp.quniform('subsample', 0.4, 1, 0.01),
            'max_depth': hp.quniform('max_depth', 3, 30, 1),
            'learning_rate': hp.uniform('learning_rate', 0.0001, 0.1),
            'gamma': hp.quniform('gamma', 0.9, 1, 0.01)}
        if self.custom_params_space is None:
            pass
        else:
            _space = self.custom_params_space

        # Estimate best params using X, y
        opt = optimize_params(X, y, _space, validation_split, self.n_jobs,
                              self.class_weight, self.verbose)

        # Instantiate `xgboost.XGBClassifier` with optimized parameters
        best_ = XGBClassifier(n_estimators=5000,
                              max_depth=int(opt['max_depth']),
                              learning_rate=opt['learning_rate'],
                              min_child_weight=opt['min_child_weight'],
                              subsample=opt['subsample'],
                              gamma=opt['gamma'],
                              colsample_bytree=opt['colsample_bytree'],
                              n_jobs=self.n_jobs)

        if self.class_weight is not None:
            sample_weight = compute_sample_weight(self.class_weight, y)
        else:
            sample_weight = None

        best_.fit(X, y,
                  sample_weight=sample_weight,
                  eval_set=[(X, y)],
                  sample_weight_eval_set=[sample_weight],
                  eval_metric='auc',
                  early_stopping_rounds=10,
                  verbose=self.verbose)
        self.best_estimator_ = best_
        return self

    def predict(self, X):
        """Predict labels with trained XGB model.

        Parameters
        ----------
        X : ndarray, shape (n_samples, n_features)

        Returns
        -------
        output : ndarray, shape (n_samples,) or (n_samples, n_labels)
        """
        if not hasattr(self, 'best_estimator_'):
            raise NotFittedError('Call `fit` before `predict`.')
        else:
            return self.best_estimator_.predict(X)

    def predict_proba(self, X):
        """Predict labels probaiblities with trained XGB model.

        Parameters
        ----------
        X : ndarray, shape (n_samples, n_features)

        Returns
        -------
        output : ndarray, shape (n_samples,) or (n_samples, n_labels)
        """
        if not hasattr(self, 'best_estimator_'):
            raise NotFittedError('Call `fit` before `predict_proba`.')
        else:
            return self.best_estimator_.predict_proba(X)

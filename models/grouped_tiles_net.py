"""Neural network which operates on groups of 1000 tiles."""

from keras.layers import (Input, Conv1D, TimeDistributed, Reshape,
                          BatchNormalization, ReLU, Dropout, Dense)
from keras.models import Model


def build_model(n_features):
    """Implementation of the proposed model.

    Parameters
    ----------
    n_features : int
        Number of features extracted for each tile.

    Returns
    -------
    model : instance of `keras.engine.training.Model`
    """
    input_layer = Input(shape=(1000, n_features, 1), name='input_layer')
    out_conv = TimeDistributed(Conv1D(1,
                                      kernel_size=n_features,
                                      strides=1,
                                      padding='valid',
                                      use_bias=True,
                                      name='conv1d'))(input_layer)
    out_reshaped = Reshape(target_shape=(1000,))(out_conv)
    out_norm = BatchNormalization()(out_reshaped)
    out_relu = ReLU()(out_norm)
    out_dropout = Dropout(0.5)(out_relu)
    output_layer = Dense(units=2, activation='softmax')(out_dropout)

    # Return model
    model = Model(inputs=input_layer, outputs=output_layer)
    return model

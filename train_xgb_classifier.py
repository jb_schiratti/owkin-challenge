"""Train XGBClassifier using ResNet features."""

import pickle

import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import (StratifiedKFold, cross_val_score,
                                     StratifiedShuffleSplit)
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from data_loaders import (get_strongly_annotated_training_data, PATH_DATA,
                          get_training_resnet_features_fnames,
                          get_weakly_annotated_training_data)
from models.optimized_xgb import OptimizedXGB
from utils.transforms import probabilities_transformer

# Load data
X, y, _ = get_strongly_annotated_training_data()

# Do multiple CV runs
clf = OptimizedXGB(n_jobs=-1)
n_runs = 10
roc_auc_scores = []
for num_run in range(n_runs):
    cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=num_run)
    scores = cross_val_score(estimator=clf,
                             X=X,
                             y=y,
                             scoring='roc_auc',
                             cv=cv)
    roc_auc_scores.append(scores)

# Mean/std of CV scores
roc_auc_scores = np.asarray(roc_auc_scores)
roc_auc_scores = roc_auc_scores.reshape((-1, 1))
print('AUC score = {} (+/- {})'.format(np.mean(roc_auc_scores),
                                       np.std(roc_auc_scores)))

# Fit the model on the whole train data
clf.fit(X=X, y=y)
clf_fname = PATH_DATA.joinpath('saved_models', 'best_xgb.pkl')
with open(clf_fname, 'wb') as fb:
    pickle.dump(clf, fb)

# Make predictions on data with weak labels using the fitted classifier
X_preds, y_preds = [], []
_, fnames_weak = get_training_resnet_features_fnames()
for fname in fnames_weak:
    _X, _y = get_weakly_annotated_training_data(fname=fname)
    probas = clf.predict_proba(_X)[:, 1]
    X_preds.append(probas)
    y_preds.append(_y)

# Transform the predicted class probabilities using a transformer
X_new = []
tr = probabilities_transformer()
for j in range(len(X_preds)):
    _X = X_preds[j].reshape((-1, 1))
    new_features = tr.transform(_X)
    X_new.append(new_features.reshape((1, -1)))
X_new = np.concatenate(X_new, axis=0)
y_new = np.array(y_preds)

# Score the final classifier
_cv = StratifiedShuffleSplit(n_splits=10, test_size=0.3, random_state=42)
final_clf = Pipeline([('scaler', StandardScaler()),
                      ('lr', LogisticRegression(C=1,
                                                max_iter=2000,
                                                penalty='l2',
                                                class_weight='balanced',
                                                solver='liblinear'))])
# Single round of CV
final_scores = cross_val_score(estimator=final_clf,
                               X=X_new,
                               y=y_new,
                               cv=_cv,
                               scoring='roc_auc')
print('Final ROC AUC scores = {} (+/- {})'.format(np.mean(final_scores),
                                                  np.std(final_scores)))

# Fit the 'final classifier' on the weakly labeled data
final_clf.fit(X=X_new, y=y_new)
clf_fname = PATH_DATA.joinpath('saved_models', 'final_clf_xgb.pkl')
with open(clf_fname, 'wb') as fb:
    pickle.dump(final_clf, fb)

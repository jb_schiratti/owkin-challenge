"""Predictions on test data using a fitted XGB classifier."""

import pickle

import numpy as np
import pandas as pd

from data_loaders import get_test_resnet_features_fnames, PATH_DATA
from utils.submission import create_submission
from utils.transforms import probabilities_transformer

# Load classifiers
clf_fname = PATH_DATA.joinpath('saved_models', 'best_lr.pkl')
with open(clf_fname, 'rb') as fb:
    clf = pickle.load(fb)

final_clf_fname = PATH_DATA.joinpath('saved_models', 'final_clf_lr.pkl')
with open(final_clf_fname, 'rb') as fb:
    final_clf = pickle.load(fb)

# Make prediction on test data
test_fnames = get_test_resnet_features_fnames()
tr = probabilities_transformer()
predicted_labels, predicted_id = [], []
for fname in test_fnames:
    current_id = fname.stem.split('_')[1]
    features = np.load(fname)
    _probas = clf.predict_proba(features[:, 3:])[:, 1]
    _new_features = tr.transform(_probas.reshape((-1, 1)))
    _new_features = _new_features.reshape((1, -1))
    y_pred_proba = final_clf.predict_proba(_new_features)[:, 1]

    predicted_labels.append(y_pred_proba[0])
    predicted_id.append(current_id)

# Create submission file
results = pd.DataFrame(data={'ID': predicted_id, 'Target': predicted_labels})
create_submission(df=results, fname=PATH_DATA.joinpath('results_xgb.csv'))
